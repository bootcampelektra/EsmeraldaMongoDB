const express = require('express');
const mongoose = require('mongoose');
const rutasProfesores = require('./routes/profesores');
const rutasAreasVerdes = require('../src/routes/areasVerdes');
const rutasSalones = require('../src/routes/salon');
require("dotenv").config();




const app = express();
const port = 3000;

app.use(express.json());

//middleware
app.use('/api', rutasProfesores);

app.use(express.json());

//Rutas
app.use('/api', rutasAreasVerdes);
app.use('/api', rutasSalones);
app.use('/api', rutasProfesores);


app.get('/', (req,res) => res.send('Hello World') );


mongoose.connect(process.env.DB_URI)
.then(() =>{
    console.log("Conexión éxitosa");
}).catch((error) =>{
    console.groupCollapsed(error);
})


app.listen(port, () =>  console.log(`Servidor arriba en el puerto: ${port}`))



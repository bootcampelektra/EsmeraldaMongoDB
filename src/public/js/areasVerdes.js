function obtenerAreaVerde() {

 
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
      };
      
      fetch("http://localhost:3000/api/areasVerdes/todos", requestOptions)
        .then(response => response.text())
        .then(result => {
            let datos = JSON.parse(result)
            console.log(datos);
            
            const table = document.getElementById("areasVerdes");
            
            datos.forEach((e,i) => {    //< ---  recorremos data
            
              let tr = document.createElement("tr"); //< ---  creamos una fila
              
              let td = document.createElement("td"); //< ---  Hacemos columna index dentro de la fila
              td.classList.add("index");
              td.innerHTML = i+1;
              tr.appendChild(td); //< --- Agregamos la columna en la fila
            
              for (p in e) {  //< ---  recorremos cada propiedad de cada elemento
            
                let td = document.createElement("td"); //< ---  Hacemos columna dentro de la fila
                td.classList.add(p);//<-- le podemos agregar a toda la columna la misma clase
                td.innerHTML = e[p]; 
            
                tr.appendChild(td); //< --- Agregamos la columna en la fila
            
              }
            
              table.appendChild(tr); //< --- Agregamos la fila a la tabla
            
            });   

        })
        .catch(error => console.log('error', error));
}
const areaVerdeSchema = require ('../models/areasVerdes');
const express = require('express');
const cors = require('cors');

const router = express.Router();
router.use(cors());

// GET
router.get('/areasVerdes/todos', (req, res) => {
    areaVerdeSchema.find({}, (error, docs) =>{
        if(!error){    
        res.json(docs);
        }else{
          res.json(error);  
        }
    })
});

router.get('/areasVerdes/solo/:id', (req,res) =>{
    const id = req.params.id;
    areaVerdeSchema.findById(id, (error, docs) => {
        if(!error){
            res.json(docs);
        }else{
            res.json(error);
        }
    })
})

// POST

router.post('/areasVerdes/guardar',  (req, res) => {
    const {nombre, ubicacion}= req.body;
    
    if (!nombre) {
        return res.status(401).json({
          msg: "No puede ir el campo nombre vacío"
        });
    
      } 


      if (!ubicacion) {
        return res.status(401).json({
          msg: "No puede ir el campo ubicación vacío"
        });
    
      }

    const areaV = new areaVerdeSchema({
        nombre: nombre,
        ubicacion: ubicacion
    });
    areaV
        .save()
        .then((data) => {
            res.json({
                msg: "Area verde agregada",
                data: data
            })
        }).catch((error) => {
            res.json({
                mensaje: error
            })
        }) 
})


// PUT


router.put('/areaVerdes/modificar/:id', (req, res) => {
    const id = req.params.id;
    const {nombre, ubicacion}= req.body;


    if (!nombre) {
        return res.status(401).json({
          msg: "No puede ir el campo nombre vacío"
        });
    
      } 


      if (!ubicacion) {
        return res.status(401).json({
          msg: "No puede ir el campo ubicación vacío"
        });
    
      }

    areaVerdeSchema.findByIdAndUpdate(id,{
        nombre: nombre,
        ubicacion: ubicacion
    },
    function (err, docs) {
    if (err){
        res.json({
            msg: err
        })
    }
    else{
        res.json({
            msg: "Area Verde Actuaizada",
            data: docs
        });
    }
});
})


// DELETE 

router.delete('/areasVerdes/eliminar/:id', (req, res) => {
    const id = req.params.id;
    areaVerdeSchema.findByIdAndDelete(id, function (err, docs) {
        if (err){
            res.json({
                msg: err
            })
        }
        else{
            res.json({
                msg: "Area Verde Eliminada",
                data: docs
            });
        }
    });
    
})


module.exports = router 
 const express = require('express');
 const salonSchema = require('../models/salones');
 const cors = require('cors');

 const router= express.Router()

 //Agregar salon

 router.post('/guardarSalon', (req, res)=>{

    const sal = salonSchema(req.body);
    sal
    .save()
    .then((data) => {
        
    }).catch((err) => {
        res.json({mensaje: err})
    });
 });

 //Buscar Salones

 router.get('/mostrarSalones', async(req, res)=>{
    try {
        const sal = await salonSchema.find()
        console.log(sal)
        res.json(sal)

        
    } catch (error) {
        res.json(error)
        
    }
 })

 //Buscar salon 

 router.get('/mostrar/:id', async (req, res)=>{
    try {
        const {id} = req.params;
        const sal = await salonSchema.findById(id)

        res.json(sal)
        
    } catch (error) {
         res.json(error);
    }
 })


 //Cambiar salon

 router.put('/cambiarSalon/:id', (req, res)=>{
    const {id} = req.params;
    const sal = salonSchema(req.body);
    console.log(sal)

    salonSchema.findByIdAndUpdate(
        id, {$set:{
            codigo: sal.codigo,
            area: sal.area
        }},
        function (err, data){
            if(err){
                console.log(err);
                res.json({mensaje: err});
            }
            else{
                console.log("Actualizado: ", data);
                res.json({msg: "Salon actualizado"})
            }
        });
 });

 //Borrar salon

 router.delete('/borrarSalon/:id', async(req, res)=>{
    try {
        const {id}= req.params;
        const sal= await salonSchema.findByIdAndDelete(id)
        res.json("Se borro el usuario")
    } catch (error) {
        res.json(error)
         }
 })





 module.exports= router;

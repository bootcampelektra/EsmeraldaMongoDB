const router = require("express").Router();
const profesorSchema = require("../models/profesores");
const cors = require('cors');
router.use(cors());


router.get("/profesores", async (req, res) => {
  await profesorSchema
    .find()
    .exec()
    .then((data) => {
      res.json(data);
    })
    .catch((err) => {
      res.json({ mensaje: err });
    });
});

router.post("/profesor", async (req, res) => {
  const profesor = await profesorSchema(req.body);
  console.log(profesor);
  if (!profesor.nombre || !isNaN(profesor.nombre) === true ) {
    return res.json({ mesaje: "Falta nombre" });
    
  }
  
  if (!profesor.apellidoP || !isNaN(profesor.apellidoP) === true) {
    return res.json({ mesaje: "Falta apellido paterno" });
  }
  if (!profesor.apellidoM || !isNaN(profesor.apellidoM) === true) {
    return res.json({ mesaje: "Falta apellido materno" });
  }
  if (!profesor.rfc || !isNaN(profesor.rfc) === true) {
    return res.json({ mesaje: "Falta rfc" });
  }
  if (!profesor.materiaPartir || !isNaN(profesor.materiaPartir) === true) {
    return res.json({ mesaje: "Falta materia" });
  }

  if (!profesor.grupo || !isNaN(profesor.grupo) === true) {
    return res.json({ mesaje: "Falta grupo" });
  }

  profesor
    .save()
    .then((data) => {
      res.json({ mesaje: "Registrado" });
    })
    .catch((err) => {
      res.json({ mensaje: err });
    });
});

router.put("/profesor/:id", async (req, res) => {
  const { id } = req.params;
  if (!id) { return res.json({ mesaje: "Falta nombre" });  }

  const datos = await profesorSchema(req.body);
  if (!datos.nombre || !isNaN(datos.nombre) === true ) {
    return res.json({ mesaje: "Falta nombre" });
    
  }
  
  if (!datos.apellidoP || !isNaN(datos.apellidoP) === true) {
    return res.json({ mesaje: "Falta apellido paterno" });
  }
  if (!datos.apellidoM || !isNaN(datos.apellidoM) === true) {
    return res.json({ mesaje: "Falta apellido materno" });
  }
  if (!datos.rfc || !isNaN(datos.rfc) === true) {
    return res.json({ mesaje: "Falta rfc" });
  }
  if (!datos.materiaPartir || !isNaN(datos.materiaPartir) === true) {
    return res.json({ mesaje: "Falta materia" });
  }

  if (!datos.grupo || !isNaN(datos.grupo) === true) {
    return res.json({ mesaje: "Falta grupo" });
  }
  profesorSchema
    .findByIdAndUpdate(id, {
      $set: {
        nombre: datos.nombre,
        apellidoP: datos.apellidoP,
        apellidoM: datos.apellidoM,
        rfc: datos.rfc,
        materiaPartir: datos.materiaPartir,
        grupo: datos.grupo,
      },
    })
    .then((data) => {
      res.json({ mesaje: "Actualizado" });
    })
    .catch((err) => {
      res.json({ mensaje: err });
    });
});

router.delete("/profesor/:id", async (req, res) => {
  try {
    const { id } = req.params;
    if (!id) { return res.json({ mesaje: "Falta nombre" });  }
    const eliminado = await profesorSchema.findByIdAndDelete(id);
    res.json({ mensaje: "Eliminado" });
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;

const mongoose = require('mongoose');

const areasVerdesSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true
    },
    ubicacion: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('AreasVerdes', areasVerdesSchema);
const mongoose = require('mongoose')    
const profesores = mongoose.Schema({
    nombre : {
        type : String,
        default : 'default txt',
    },
    apellidoP : {
        type : String,
        default : 'default txt',
    },
    apellidoM : {
        type : String,
        default : 'default txt',
    },
    rfc : {
        type : String,
        default : 'default txt',
    },
    materiaPartir : {
        type : String,
        default : 'default txt',
    },
    grupo : {
        type : String,
        default : 'default txt',
    }

})
module.exports =  mongoose.model( 'profesores' , profesores)

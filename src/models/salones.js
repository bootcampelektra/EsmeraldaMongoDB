const mongodb = require('mongoose')

const salonSchema = new mongodb.Schema({
    "codigo": {type: String, required: true},
    "area": {type: String, required: true}
})

module.exports= mongodb.model('Salon', salonSchema)